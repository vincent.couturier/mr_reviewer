# prerequisite 
you should have the following variables provided
 - `CHECKED_OUT_LOCAL_REPOSITORY`: contains the path where you checked out the branch of the MR
 - `GITLAB_ACCESS_TOKEN`: the access token you created on gitlab to be able to get content of the MR this tool have to analyse
 - `GITLAB_HOST`: (optional) in case you use a specific GITLAB_HOST you can precise its url here. By default the tool use https://gitlab.com

Before using the command you should run `yarn install`

# how to call MR Reviewer
`your_project_path` refers to the path in the url just before the "-/merge_requests/" section of the mr url

`your_mr_id refers` to the id available juste after the "-/merge_requests/" section of the mr url
## to analyse the MR

```
npx ts-node index.ts ${your_project_path} ${your_mr_id} 
```
in case you want to avoid pushing comment to gitlab MR you can disable this push by using `--disableMRComment`
```
npx ts-node index.ts ${your_project_path} ${your_mr_id} --disableMRComment
```
In case you want to precise a prompt you can add it at the end of the command line

## to analyse the MR and generate an image related to the result of this analysis (just for fun.... 🤠)
```
npx ts-node index.ts ${your_project_path} ${your_mr_id} --genImage
```

## How it works
```mermaid
flowchart
    mr_reviewer["mr_reviewer"]
    subgraph s1["Gitlab MR"]
        s11["talent-form.html\n+"]
        s12["Talent.kt\n++--"]
        s13["talent.ts\n++--"]
        s14["readme.md\n++++++--"]
    end
	mr_reviewer---|"mr api"| s1
	s1--- gitlab["gitlab"]
	
	mr_reviewer--- s2
	s2--- openai_model["openai gpt4 model"]
    subgraph s2["completion api"]
        s21("function\n<div style="background-color:#1e1f22;color:#bcbec4"><pre style="font-family:'JetBrains Mono',monospace;font-size:9,8pt;"><span style="color:#6aab73;">getCodeByFilePath</span></pre>\n")
    end
    mr_reviewer--- source_code_files["source code files"]
    s2-.- source_code_files["source code files"]
    mr_reviewer---> result
    subgraph result["MR Analysis"]
      sc["Summary of changes"]
      improvement_proposal["Improvement proposal"]
    end
    
```