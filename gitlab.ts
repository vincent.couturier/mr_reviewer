import axios from "axios";
import * as crypto from "crypto";

const gitlabHost = process.env['GITLAB_HOST'] ?? 'https://gitlab.com';
const gitlabAccessToken: string = process.env['GITLAB_ACCESS_TOKEN'] as string;

export interface partialMR {
    diff_refs: { base_sha: string, head_sha: string, start_sha: string }
}

const headers = {
    'PRIVATE-TOKEN': gitlabAccessToken
};

export async function getMrDetails(projectId: string, mergeRequestId: string) {
    try {
        const url = `${gitlabHost}/api/v4/projects/${encodeURIComponent(projectId)}/merge_requests/${mergeRequestId}/changes`;
        const response = await axios.get(url, {headers});
        if (response.status >= 400) {
            throw new Error(`Error: ${response.status}`);
        }
        console.log(response.data)
        return response.data
    } catch (error) {
        console.error(error);
    }
}

export async function addGlobalComment(projectId: string, mergeRequestId: string, mrDetails: partialMR, comment: string) {
    try {
        const url = `${gitlabHost}/api/v4/projects/${encodeURIComponent(projectId)}/merge_requests/${mergeRequestId}/notes`;
        const response = await axios.post(url, {
            body: formatMessageOfMRReviewer(comment),
        }, {headers});
        if (response.status >= 400) {
            throw new Error(`Error: ${response.status} with data: ${response.data}`);
        }
        console.log(response.data)
        return response.data
    } catch (error) {
        console.error(error);
    }
}

function formatMessageOfMRReviewer(text: string): string {
    return `[🤖mr_reviewer]
${text}
`
}

export async function addMRThread(projectId: string, mergeRequestId: string, mrDetails: partialMR, oldFilePath: string, newFilePath: string,  comment: string, oldLineNumber: number|null, newLineNumber: number|null) {
    try {
        const url = `${gitlabHost}/api/v4/projects/${encodeURIComponent(projectId)}/merge_requests/${mergeRequestId}/discussions`;
        const data = {
            body: formatMessageOfMRReviewer(comment),
            position: {
                base_sha: mrDetails.diff_refs.base_sha,
                start_sha: mrDetails.diff_refs.start_sha,
                head_sha: mrDetails.diff_refs.head_sha,
                position_type: 'text',
                old_path: oldFilePath,
                new_path: newFilePath,
                old_line: oldLineNumber,
                new_line: newLineNumber,
            }
        };
        console.log(`will add thread using following data`, data);
        const response = await axios.post(url, data, {headers});
        if (response.status >= 400) {
            throw new Error(`Error: ${response.status} with data: ${response.data}`);
        }
        console.log(response.data);
        return response.data;
    } catch (error) {
        console.error(error);
        throw error;
    }
}

function generateLineCode(filePath: string, oldLineNumber: number, newLineNumber: number): string {
    // This is a placeholder function. The actual implementation of generating a line code
    // depends on how GitLab calculates the line code hash which typically involves hashing
    // the file path and line number along with the start SHA.
    // You will need to replace this with the actual logic used by your GitLab instance.
    const filePathSha = crypto.createHash('sha1').update(filePath).digest('hex')
    return `${filePathSha}_${oldLineNumber}_${newLineNumber}`;
}