import OpenAI from 'openai';
import {addGlobalComment, addMRThread, getMrDetails, partialMR} from "./gitlab";
import {readFileSync} from "fs";
import {RunnableToolFunction} from "openai/lib/RunnableFunction";

const openai = new OpenAI({
    apiKey: process.env['OPENAI_API_KEY'], // This is the default and can be omitted
});
const context: { projectId: string, mrId: string, mrDetails: partialMR, disableMRComment: boolean } = {} as any

async function listModels() {
    const listResult = await openai.models.list({})
    console.log(`response : ${JSON.stringify(listResult, null, 2)}`)
}

let finalResult = ""

async function askQuestion(prompt: string) {
    console.log(`prompt : 
      ${prompt}
    `)
    const getCodeByFilePathRunnableFunction: RunnableToolFunction<{ path: string }> = {
        function: {
            name: "getCodeByFilePath",
            description: "find original source file content",
            parameters: {
                type: 'object',
                properties: {
                    path: {type: 'string'},
                },
            },
            function: getCodeByFilePath,
            parse: JSON.parse
        },
        type: 'function'
    }
    const addMRFeedbackRunnableFunction: RunnableToolFunction<{
        comment: string,
        oldFilePath: string,
        newFilePath: string,
        oldLineNumber: number | null,
        newLineNumber: number | null
    }> = {
        function: {
            name: "addMRThreadOnSpecificChange",
            description: "add feedback to a specific change of file of a MR located at a specific line. The file is identified by its old file path and its new file path. The line of code is identified by its old line number and its new line number. for a new line, the old line number should be null and for removed line the new line number should null",
            parameters: {
                type: 'object',
                properties: {
                    comment: {type: 'string'},
                    oldFilePath: {type: 'string'},
                    newFilePath: {type: 'string'},
                    oldLineNumber: {type: 'number'},
                    newLineNumber: {type: 'number'}
                },
            },
            function: addMRThreadOnSpecificChange,
            parse: JSON.parse
        },
        type: 'function'
    }
    const addGlobalMRCommentRunnableFunction: RunnableToolFunction<{ comment: string }> = {
        function: {
            name: "addGlobalMRComment",
            description: "add global feedback to a MR",
            parameters: {
                type: 'object',
                properties: {
                    comment: {type: 'string'},
                },
            },
            function: addGlobalMRComment,
            parse: JSON.parse
        },
        type: 'function'
    }
    const tools: RunnableToolFunction<any>[] = [getCodeByFilePathRunnableFunction, addMRFeedbackRunnableFunction, addGlobalMRCommentRunnableFunction]
    const runner = openai.beta.chat.completions.runTools(
        {
            messages: [
                {
                    role: 'system',
                    content: `You are an expert in kotlin, groovy, typescript, bash, spring boot, spring cloud, mongodb, neo4j and python. You also take care of SOLID architecture principle and practice software craftsmanship. 
                    Use function addMRThreadOnSpecificChange to create a new thread at a specific new line of the source code in order to give more useful and precise feedbacks. The old line is also needed. 
                    Use function addGlobalMRComment to indicate the result of the feedback about a MR.
                    Use function getCodeByFilePath to analyse source code and answer the following questions.`
                },
                {role: 'user', content: prompt}
            ],
            stream: true,
            model: 'gpt-4o',
            tool_choice: "auto",
            tools: tools
        }
    ).on('message', (msg) => console.log('msg', msg))
        .on('functionCall', (functionCall) => console.log('functionCall', functionCall))
        .on('functionCallResult', (functionCallResult) => console.log('functionCallResult', functionCallResult))
        .on('content', (diff) => {
            finalResult += diff;
            process.stdout.write(diff)
        });

    const chatCompletion = await runner.finalChatCompletion();
    console.log(`response : ${JSON.stringify(chatCompletion, null, 2)}`)
    console.log(`
    
    ------ final result -----
    
    ${finalResult}
    `)
    //await addGlobalMRComment({comment: finalResult})
    return chatCompletion
}

function getCodeByFilePath({path}: { path: string }) {
    const realPath = `${process.env['CHECKED_OUT_LOCAL_REPOSITORY']}/${path}`
    console.log(`getCodeByFilePath: ${realPath}`)
    try {
        return readFileSync(realPath).toString()
    } catch (error) {
        console.error(error)
        return `I cannot access to ${path}`
    }

}

async function addMRThreadOnSpecificChange({comment, oldFilePath, newFilePath, oldLineNumber, newLineNumber}: {
    comment: string,
    oldFilePath: string,
    newFilePath: string,
    oldLineNumber: number | null,
    newLineNumber: number | null
}) {
    console.log(`addMRThreadOnSpecificChange on ${oldFilePath}-${newFilePath} at ${oldLineNumber} - ${newLineNumber} : ${comment.substring(0, 10)} (${comment.length})`)
    try {
        let result
        if (context.disableMRComment) {
            result = `comment added`
            console.debug(`call to gitlab is disable`)
        } else {
            result = await addMRThread(context.projectId, context.mrId, context.mrDetails, oldFilePath, newFilePath, comment, oldLineNumber, newLineNumber)
            console.log(`addMRThreadOnSpecificChange result: ${JSON.stringify(result, null, 2)}`)

        }
        return `${result}`
    } catch (error) {
        console.error(JSON.stringify(error, null, 2))
        console.error(error)
        return `I cannot add thread ${comment} to ${oldFilePath}-${newFilePath} at line ${oldLineNumber} - ${newLineNumber}`
    }

}

async function addGlobalMRComment({comment}: { comment: string }) {
    console.log(`addGlobalMRComment : ${comment.substring(0, 10)} (${comment.length})`)
    try {
        let result
        if (context.disableMRComment) {
            result = `comment added`
            console.debug(`call to gitlab is disable`)
        } else {
            result = await addGlobalComment(context.projectId, context.mrId, context.mrDetails, comment)
            console.log(`addGlobalMRComment result: ${JSON.stringify(result, null, 2)}`)
        }
        return `${result}`
    } catch (error) {
        console.error(JSON.stringify(error, null, 2))
        console.error(error)
        return `I cannot add global comment ${comment}`
    }

}

async function createImage(prompt: string) {
    const image = await openai.images.generate({model: "dall-e-3", prompt});
    console.log(image.data);
}

async function askAssistant() {
    await openai.beta.threads.create({
        messages: []
    }, {})
}

async function main() {
    console.log("start")
    const projectId = process.argv[2]
    const mrId = process.argv[3]
    const options = process.argv[4] ?? ""
    const firstOption = options.split(" ")[0]
    const imageAnalysis = firstOption === '--genImage'
    const disableMRComment = firstOption === '--disableMRComment'
    const additionalPrompt = firstOption.startsWith("--") ? process.argv[5] ?? "" : options
    const mrDetails: partialMR = await getMrDetails(projectId, mrId)
    context.projectId = projectId
    context.mrId = mrId
    context.mrDetails = mrDetails
    context.disableMRComment = disableMRComment
    if (imageAnalysis) {
        const chatCompletion = await askQuestion(`
       Using the following gitlab merge request details : 
       \`\`\`
       ${JSON.stringify(mrDetails, null, 2)}
       \`\`\`
       could you summarize changes introduce by this MR and generate a prompt to call dall-e in order to generate an image to help understand what the new code implies ?
    `)
        const imagePrompt = chatCompletion.choices[0].message.content as string
        console.log(`image prompt : ${imagePrompt}`)
        await createImage(imagePrompt)
    } else {
        await askQuestion(`
Using the following gitlab merge request details : 
\`\`\`
${JSON.stringify(mrDetails, null, 2)}
\`\`\`

Provide a clear and concise analysis of the changes introduced by this merge request.
Summarize the impact these changes will have on existing features.
Identify any potentially problematic or suspicious code within the changes.
List specific changes that could be enhanced, and suggest new source code where applicable.
Evaluate the relevance of names used for files, classes, methods, or fields, and propose better alternatives if needed.
Directly apply your feedback to the merge request:
  - Use addGlobalMRComment to provide overarching feedback that applies to the entire MR.
  - Use addMRThreadOnSpecificChange to give detailed feedback on specific lines of code. If the exact line is not available, default to line 1.
  - Indicate the severity of each issue using emojis to convey the level of urgency or importance.
  - If you're unable to attach feedback to a specific file, ensure it's included in the global feedback.
Your analysis should be thorough, actionable, and adhere to best practices in software development.    
${additionalPrompt}
`)
    }

}

main();
